'use strict';

const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const validator = require('express-validator');
const session = require('express-session');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const MongoStore = require('connect-mongo')(session);


const indexRoute = require('./routes/index');
const authRoute = require('./routes/auth');
const helmet = require('helmet');

const app = express();
const port = process.env.PORT || 8081;

mongoose.connect('mongodb://localhost:27017/authentication', (err) => {
	if (err)
		throw err;
	console.log('connected to db: authentication');
});

app.use(helmet());
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
	secret: '*|< 0 |\/| 3 D*', // komed :)
	cookie: {
		// key: 'XSRF-TOKEN',
		secure: false,
		httpOnly: false
	},
	resave: true,
	saveUninitialized: false,
	store: new MongoStore({
		mongooseConnection: mongoose.connection,
		ttl: 60 * 60 * 2 // 2h
	})
}));
app.use(validator());

app.use('/auth', authRoute);
app.use('/', indexRoute);

app.listen(port, (err) => {
	if (err)
		throw err;
	console.log(`SERVER RUNNING ON PORT: ${port}`);
});