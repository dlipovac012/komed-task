'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const keySchema = new Schema({

    userId: { type: String, required: true },
    key: { type: String, required: true }
});

keySchema.methods.generateKey = (key) => {
    return bcrypt.hashSync(key, bcrypt.genSaltSync(13));
}

module.exports = mongoose.model('Key', keySchema);