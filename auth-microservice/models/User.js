'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const userSchema = new Schema({

	name: { type: String, required: true },
	email: { type: String, required: true },
	password: { type: String, required: true }
});

userSchema.methods.encryption = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(13));
};

userSchema.methods.validation = (password, hashed) => {
    return bcrypt.compareSync(password, hashed);
};

module.exports = mongoose.model('User', userSchema);