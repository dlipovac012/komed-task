'use strict';

const express = require('express');
const validator = require('express-validator');
const User = require('../models/User');
const Key = require('../models/Key');
const jwt = require('jsonwebtoken');
const request = require('request');

const router = express.Router();
router.use(validator());
let sessionStorageRef = {};
let failedLoginAttempts = {};

router.get('/', (req, res) => {

	res.send({});
});

router.post('/signup', (req, res) => {

	req.checkBody('name', 'Name shouldn\'t be empty').notEmpty();
	req.checkBody('email', 'Email empty').notEmpty().isEmail();
	req.checkBody('password', 'Bad password').notEmpty().len({options: [{ min: 4}]});

	req.getValidationResult()
		.then((result) => {
			
		if (result.array().length === 0) {
			
			User.findOne({'email': req.body.email}, (err, user) => {
				if (err)
					res.status(500).send('Error occured');
				else if (user) {
					res.status(400).send('User already exists');
				}
				else {
					let newUser = new User();
					let newKey = new Key();

					newUser.name = req.body.name;
					newUser.email = req.body.email;
					newUser.password = newUser.encryption(req.body.password);

					newKey.userId = newUser._id;
					newKey.key = newKey.generateKey(Math.random().toString(32).substring(16));
					
					newUser.save((err, user) => {
						
						if (err)
							throw err;
						else {
							newKey.save((err) => {
								if (err)
									throw err;
								res.status(200).send('User created');
							})
						}
					});
				}
			});
		}
		else {
			res.status(400).json(result.array());
		}
	});
});

router.post('/login', (req, res) => {

	let ipAddress = req.connection.remoteAddress.match(/(?:\d{1,3}\.){3}\d{1,3}/)[0];

	req.checkBody('email', 'Email empty').notEmpty().isEmail();
	req.checkBody('password', 'Badly typed password').notEmpty().len({options: [{ min: 4}]});

	req.getValidationResult()
		.then((result) => {

			if (result.array().length === 0) {

				User.findOne({'email': req.body.email}, (err, user) => {
					if (err)
						res.send(500).send('Error occured');
					else if (!user) {
						res.status(400).send('No user found');
					}
					else {

						if (!user.validation(req.body.password, user.password)) {

							// against brute force attacks
							let f = failedLoginAttempts[ipAddress] = failedLoginAttempts[ipAddress] || {count: 0, nextTry: new Date()};
							++f.count;
							f.nextTry.setTime(Date.now() + 2000 * f.count);

							res.status(401).send('Wrong password');
						}
						else {

							delete failedLoginAttempts[ipAddress];
							Key.findOne({'userId': user._id}, (err, key) => {
								if (err)
									res.status(500).send(err);
								const jwtToken = jwt.sign({ email: user.email, key: key.key }, user.password,
								{
									expiresIn: '1h'
								});
								if (!req.session.user) {
									req.session['user'] = {
										userId: user._id,
										hashKey: user.password
									}
								}
								sessionStorageRef = JSON.parse(JSON.stringify(req.session));
								res.status(200).json({token: jwtToken});
							});
						}
					}
				});
			}
			else {
				res.status(400).json(result.array());
			}
		});
});

router.post('/authenticate', (req, res) => {

	if (Object.keys(sessionStorageRef).length !== 0) {
		jwt.verify(req.body.token, sessionStorageRef.user.hashKey, (err, decoded) => {
		if (err) {
			console.log(err); //this is just for dev purpose, good thing would be to store errors in db or some log files
			res.status(500).send(err);
		}
		else {
			res.status(200).send(decoded);
		}
	});
	}
});

router.get('/logout', (req, res) => {

	//req.session.destroy();
	if (req.session.user)
		delete req.session.user;
	sessionStorageRef = {};
	res.status(200).send('user logged out');
});

let MINS10 = 600000, MINS30 = 3 * MINS10; // preventing memory leaks
setInterval(() => {
    for (let ip in failedLoginAttempts) {
        if (Date.now() - failedLoginAttempts[ip].nextTry > MINS10) {
            delete failedLoginAttempts[ip];
        }
    }
}, MINS30);

module.exports = router;