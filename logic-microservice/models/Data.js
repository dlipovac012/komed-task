'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dataSchema = new Schema({

    data: { type: String, required: true }
});

module.exports = mongoose.model('Data', dataSchema);