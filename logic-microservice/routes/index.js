'use strict';

const express = require('express');
const crypto = require('crypto-js');
const sha256 = require("crypto-js/sha256");

const router = express.Router();

router.get('/', (req, res) => {

	const text = crypto.AES.encrypt('tekstic', 'sifra');
	console.log(sha256(text));

	const detext = crypto.AES.decrypt(text.toString(), 'sifra');
	
	const decrypted = detext.toString(crypto.enc.Utf8);

	console.log(decrypted);

	res.send('index logic page');
});

module.exports = router;