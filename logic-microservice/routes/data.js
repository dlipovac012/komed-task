'use strict';

const express = require('express');
const request = require('request');
const CryptoJS = require("crypto-js");
const Data = require('../models/Data');

const router = express.Router();
let sessionStorageRef = {};

router.get('/', (req, res) => {
	
});

router.get('/clear-session', (req, res) => {

	req.session.destroy();
	sessionStorageRef = {};

	// if (req.session.info)
	// 	delete req.session.info;

	res.send('session deleted');
});

router.post('/check-authentication', (req, res) => {

	request({ method: 'post', body: { token: req.body.token }, json: true, url: 'http://localhost:8081/auth/authenticate'}, (err, response, body) => {
		if (err)
			res.status(500).send(err);
		else if (response.statusCode === 200) {

			if (!req.session.info) {
				req.session.info = {
					email: body.email,
					hashKey: body.key 
				}
			}
			sessionStorageRef = JSON.parse(JSON.stringify(req.session));
			res.json('user is signed in');
		}
		else 
			res.status(401).send('user not logged in');
	});
});

router.post('/write-data', (req, res) => {

	let encryptedData = CryptoJS.AES.encrypt(JSON.stringify(req.body), sessionStorageRef.info.hashKey);

	let data = new Data();

	data.data = encryptedData;
	data.save((err, data) => {
		if (err)
			res.status(500).send('data could not be saved');
		else
			res.status(200).send('encrypted data saved');
	})
 
	
});

router.get('/read-data', (req, res) => {

	Data.find((err, data) => {

		if (err)
			res.status(500).send('error retrieving data from logic db');
		else if (!sessionStorageRef.info) 
			res.status(200).send("User not signed in"); // data[0].data returns encrypted object from database
		else if (!data[0].data)
			res.status(400).send('no data entry');
		else {

			// just for the sake of simplicity, i've saved just 1 data entry (encrypted) and i am retrieving only that one

			if (sessionStorageRef.info !== undefined) {
				let bytes  = CryptoJS.AES.decrypt(data[0].data, sessionStorageRef.info.hashKey);
				let plaintext = bytes.toString(CryptoJS.enc.Utf8);

				res.status(200).send(plaintext);
			}
		}
	});
});

module.exports = router;