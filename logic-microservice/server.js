'use strict';

const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const indexRoute = require('./routes/index');
const dataRoute = require('./routes/data');
const helmet = require('helmet');

const app = express();
const port = process.env.PORT || 8082;

mongoose.connect('mongodb://localhost:27017/logicdb', (err) => {
	if (err)
		throw err;
	console.log('connected to db: logic');
});

app.use(helmet());
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
	secret: '*D 3 |\/| 0 >|*',
	cookie: {
		// key: 'XSRF-TOKEN',
		secure: false,
		httpOnly: false
	},
	resave: false,
	saveUninitialized: false,
	store: new MongoStore({
		mongooseConnection: mongoose.connection,
		ttl: 60 * 60 * 2 // 2h
	})
}));

app.use('/logic', dataRoute);
app.use('/', indexRoute);

app.listen(port, (err) => {
	if (err)
		throw err;
	console.log(`SERVER RUNNING ON PORT: ${port}`);
});