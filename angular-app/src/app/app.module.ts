import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { PolymerModule } from '@codebakery/origami';
import { IronElementsModule, PaperElementsModule } from '@codebakery/origami/lib/collections'

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SigningComponent } from './components/signing/signing.component';
import { DataComponent } from './components/data/data.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { LogoutComponent } from './components/logout/logout.component';

import { SignupService } from './services/signup.service';
import { LoginService } from './services/login.service';
import { LogoutService } from './services/logout.service';
import { AuthenticateService } from './services/authenticate.service';
import { WriteService } from './services/write.service';
import { ReadService } from './services/read.service';



const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'signup', component: SigningComponent },
  { path: 'login', component: LoginComponent },
  { path: 'data', component: DataComponent },
  { path: 'error', component: ErrorComponent },
  { path: 'logout', component: LogoutComponent },

  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  { path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SigningComponent,
    DataComponent,
    LoginComponent,
    HomeComponent,
    ErrorComponent,
    LogoutComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    RouterModule.forRoot(
      appRoutes
      // { enableTracing: true }
    ),
    FormsModule,
    BrowserModule,
    PolymerModule.forRoot(),
    IronElementsModule,
    PaperElementsModule,
    HttpModule
  ],
  providers: [
    SignupService,
    LoginService,
    LogoutService,
    AuthenticateService,
    WriteService,
    ReadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
