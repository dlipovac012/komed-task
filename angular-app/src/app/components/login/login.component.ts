import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { CookieService } from 'angular2-cookie/core';
import bcrypt from 'bcryptjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.user = {
      name: '',
      email: '',
      password: ''
    };
  }

  loginUser() {

    this.loginService.userLogin(this.user).subscribe(data => {

      //this.cookie['token'] = JSON.parse(data.text()).token;
      window.localStorage.setItem('token', JSON.parse(data.text()).token);
      this.router.navigate(['/data']);
    }, err => {
      //console.log(err);

      if (err.status === 400) {
        alert(err._body);
      }
      if (err.status === 401)
        this.router.navigate(['/error']);
    });
  }

  cancelLogin() {
    this.user = {
      name: '',
      email: '',
      password: ''
    };
  }

}

interface User {
  name: string;
  email: string;
  password: string;
}

