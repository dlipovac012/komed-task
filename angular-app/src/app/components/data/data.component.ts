import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../../services/authenticate.service';
import { ReadService } from '../../services/read.service';
import { WriteService } from '../../services/write.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  user: User;
  data_field: string;

  constructor(private authService: AuthenticateService, private readService: ReadService, private writeService: WriteService) { }
s
  ngOnInit() {

    this.data_field = '';
    
    this.user = {
      email: '',
      data: ''
    }

    this.authService.authenticateUser({token: localStorage.getItem('token')});
  }

  writeData() {
    
    this.writeService.writeData(this.user).subscribe(data => {

    }, err => {

    });
  }

  readData() {

    this.readService.readData().subscribe(data => {

      this.data_field = data.text().toString();
    }, err => {

      console.log(err);
    });
  }
}

interface User {
  email: string;
  data: string;
}