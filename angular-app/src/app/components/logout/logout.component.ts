import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogoutService } from '../../services/logout.service';
import { AuthenticateService } from '../../services/authenticate.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private logoutService: LogoutService, private router: Router, private authService: AuthenticateService) { }

  ngOnInit() {
    this.logoutService.userLogout();
  }

}
