import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupService } from '../../services/signup.service';
import bcrypt from 'bcryptjs';

@Component({
  selector: 'app-signing',
  templateUrl: './signing.component.html',
  styleUrls: ['./signing.component.css']
})
export class SigningComponent implements OnInit {

  user:User;
  rep_password: string;

  constructor(private signupService: SignupService, private router: Router) { }

  ngOnInit() {
    this.user = {
      name: '',
      email: '',
      password: ''
    };
    this.rep_password = '';
  }

  submitUser() {

    let newUser:User;

    if (this.user.password === this.rep_password) {
        newUser = {
        name: this.user.name,
        email: this.user.email,
        password: this.user.password //(this.user.password, bcrypt.genSaltSync(13))
      }
    }
    else {

    }

    this.signupService.userSignup(newUser).subscribe(data => {
      //console.log(data);
      if (data.status === 200) {
        this.router.navigate(['/']);
      }
    }, err => {

      //console.log(err);
      if (err.status === 400) {
        alert(err._body);
      }
      if (err.status === 401)
        this.router.navigate(['/error']);
    });;
  }

  cancelSubmit() {
    this.user = {
      name: '',
      email: '',
      password: ''
    };
    this.rep_password = '';
  }
}

interface User {
  name: string;
  email: string;
  password: string;
}
