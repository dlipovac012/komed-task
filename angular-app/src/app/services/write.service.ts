import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class WriteService {

  constructor(public http: Http) { }

  writeData(data) {

    return this.http.post('/logic/write-data', data);
  }
}
