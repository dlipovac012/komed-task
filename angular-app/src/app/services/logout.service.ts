import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LogoutService {

  constructor(public http: Http) { }

  userLogout() {

    this.http.get('/auth/logout').subscribe(data => {

      // console.log(data);
    }, err => {

      // console.log(err);
    });

    this.http.get('/logic/clear-session').subscribe(data => {

      // console.log(data);
    }, err => {

      // console.log(err);
    });
  }
}
