import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  constructor(public http: Http) {

  }

  userLogin(user) {
    return this.http.post('/auth/login', user);
  }

}
