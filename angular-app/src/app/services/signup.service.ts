import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SignupService {

  constructor(public http:Http) {
  }

  checkConnection() {
  }

  userSignup(user) {

    return this.http.post('/auth/signup', user);
  }
}
