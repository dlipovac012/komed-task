import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class AuthenticateService {

  constructor(public http: Http) { }

  authenticateUser(token) {

    this.http.post('/logic/check-authentication', token).subscribe(data => {

      // console.log(data);
    }, err => {

      // console.log(err);
    });
  }
}
