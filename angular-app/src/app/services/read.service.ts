import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ReadService {

  constructor(public http: Http) { }

  readData() {

    return this.http.get('/logic/read-data');
  }
}
